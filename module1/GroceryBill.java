package com.module1;

public class GroceryBill {
    public static void main(String[] args) {
        //declare 10 variables to store different kinds of items that I am procuring
        /*float item1 = 1.89f;
        float item2  = 2.33f;
        float item3  = 5.33f;
        float item4  = 6.33f;
        float item5 ; // initialize the variables - not using the variable - will create garbage...

        float item6 = 62.55f;
        float item7 = 27.55f;
        float item8 = 28.55f;
        float item9 = 288.55f;
        float item10 = 28.55f;*/

        float item[] = new float[8];// 0-99
        item[0]=1.89f;
        item[1]=45.65f;
        item[2]=56.55f;
        item[3]=76.55f;
        item[4]=86.55f;
        item[5]=96.55f;
        item[6]=26.55f;
        item[7]=36.55f;

        //double sum = item[0]+item[1]+item[2]+item[3]+item[4]+item[5]+item[6]+item[7];

       //initializing the variable
        //0+1.89 --> sum = 1.89
        //1.89+45.65 -->sum = 48.99
        //48.99+56.55-->Sum =
        double sum = 0;
        for (int i = 0; i <=7 ; i++) {
            sum = sum+item[i];
        }
        System.out.println();
        System.out.println("The total sum is " + sum);

//for each --> datatype dummyvariabe arrayvariablename
        for (float aravindh:item
             ) {
            System.out.println(aravindh);
        }
    }
}
