package com.module1;

public class TestWhile {
    public static void main(String[] args) {

        /*while (condition){
        increment / decrement - statemetn to be executed
         }
          // Type number 1 to 100
        */

        int i=1;
        while (i>=1){
            System.out.println(i);
            i++;
        }
    }
}
