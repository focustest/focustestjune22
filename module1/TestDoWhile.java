package com.module1;

public class TestDoWhile {
    public static void main(String[] args) {
        /*
        do{
        //code to be executed
        }while(condition);
         */
        //It runs atleast one iteration

        //ask : type from 1-to 10


        int i =1;
        do {
            i++;
            System.out.println(i);
        }while (i<=100);

        int j=1;
        do{
            System.out.println("Hi Both...");
            j++;
        }while(j<=10);

    }
}
